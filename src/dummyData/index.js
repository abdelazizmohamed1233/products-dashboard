export const productsList=[
    {id:1,name:'Ekobel' ,dimensions:'52 X 50 X 88 cm',stock:'in stock' ,avatar:'images/ekobel.jpg',avatar2:'images/ekobel@2x.jpg' ,price:'1500 $'},

    {id:2,name:'Gronlid' ,dimensions:'30 X 40 X 55 cm',stock:'in stock' ,avatar:'images/gronlid.jpg' ,avatar2:'images/gronlid@2x.jpg' ,price:'2500 $'},

    {id:3,name:'Hehairro-c' ,dimensions:'32 X 50 X 58 cm',stock:'out of stock' ,avatar:'images/Hero-chair.jpg' ,avatar2:'images/Hero-chair@2x.jpg' ,price:'1200 $'},

    {id:4,name:'Ingrid' ,dimensions:'32 X 33 X 43 cm',stock:'in stock' ,avatar:'images/ingrid.jpg',avatar2:'images/ingrid@2x.jpg' ,price:'1900 $' },

    {id:5,name:'Lallerod' ,dimensions:'43 X 23 X 31 cm',stock:'out of stock',avatar:'images/lallerod.jpg' ,avatar2:'images/lallerod@2x.jpg' ,price:'1100 $'},

    {id:6,name:'Leiframe' ,dimensions:'76 X 43 X 34 cm',stock:'in stock'
    ,avatar:'images/leifrarne.jpg' ,avatar2:'images/leifrarne@2x.jpg' ,price:'900 $'},

    {id:7,name:'Shell' ,dimensions:'21 X 32 X 54 cm',stock:'out of stock',avatar:'images/shell.jpg' ,avatar2:'images/shell@2x.jpg' ,price:'2000 $'},

    {id:8,name:'Vejmon' ,dimensions:'54 X 54 X 43 cm',stock:'in stock',avatar:'images/vejmon.jpg' ,avatar2:'images/vejmon@2x.jpg' ,price:'3000 $'},

]

/* 

intro
Simple,timeless design that's still going strong after 40 years in our stores
ood to know
Wipe clean with cloth dampened in  a mild cleaner
*/
export const usersList=[
    {id:1,name:'Jhon' ,email:'Jhon@gmail.com',status:'active' },
    {id:2,name:'Michael' ,email:'Michael@gmail.com',status:'active' },
    {id:3,name:'Hunt' ,email:'Hunt@gmail.com',status:'active'  },
    {id:4,name:'Garcia' ,email:'Garcia@gmail.com',status:'active' },
    {id:5,name:'Marshall' ,email:'Marshall@gmail.com',status:'active' },
    {id:6,name:'Anderson' ,email:'Anderson@gmail.com',status:'active'
    },
    {id:7,name:'Ruiz' ,email:'Ruiz@gmail.com',status:'active'},
    {id:8,name:'Dixon' ,email:'Dixon@gmail.com',status:'active'},

]