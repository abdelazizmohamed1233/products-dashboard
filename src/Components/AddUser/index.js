
import'./index.css'
function AddUser(){
    return (
    <div className='adduser container px-5'>
        <h2 className='user-header'>Add New User</h2>
    <form action="">
    <div className="form-group my-3">
    <label for="username">Full Name</label>
    <input type="text" className="form-control" id="username"  placeholder="Enter Fullname"/>
    </div>

    <div className="form-group my-3">
    <label for="email">Email</label>
    <input type="email" className="form-control" id="email"  placeholder="Enter Email"/>
    </div>

    <div className="form-group my-3">
    <label for="phone">Phone Number</label>
    <input type="tel" className="form-control" id="phone"  placeholder="Enter Phone Number"/>
    </div>

    <div className="form-group my-3">
    <label for="adress">Address</label>
    <input type="text" className="form-control" id="adress"  placeholder="Enter Address"/>
    </div>

    <div class="form-group my-4">
    <label for="user-image">Choose an image</label>
    <br />
    
    <input type="file"  class="form-control-file" id="user-image"/>
    </div>

  <button type="submit" className="btn btn-primary">Add</button>
    </form>
          
        </div>
       
    )
}

export default AddUser;