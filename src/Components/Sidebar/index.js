
import './index.css';
import {Storefront,People,PersonAdd,AddBox} from '@material-ui/icons';
import {Link} from 'react-router-dom'
function Sidebar (){
    return (
<div className="sidebar bg-dark">
    
            <ul className="nav flex-column">
  <li className="nav-item text-light">
    <Link className="nav-link  link text-light" to="/products"><Storefront className='sidebar-icon'/>Products</Link>
  </li>
  <li className="nav-item">
    <Link className="nav-link link text-light" to="/users"><People className='sidebar-icon'/>Users</Link>
  </li>
  <li className="nav-item">
    <Link className="nav-link link text-light" to="/addproduct"><AddBox className='sidebar-icon'/>Add Product</Link>
  </li>
  <li className="nav-item">
    <Link className="nav-link link text-light " to="/adduser"><PersonAdd className='sidebar-icon'/>Add User</Link>
  </li>
  
</ul>
</div>
    )
}

export default Sidebar