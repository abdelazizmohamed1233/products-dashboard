import { usersList } from '../../dummyData'
import './index.css'

function UserDetails(props){
    let user=usersList.find((us)=>us.id==props.match.params.id)
    return (
        <div className='text-light user-details py-3'>
              <h3>{user.name}</h3>

              <div className='img-container my-5'>
          <img src={'/images/img_avatar.png'} alt="main-img" className='main-img'/>

          </div>

          <table className='table table-dark table-bordered product-table text-light '>
                
                <tr>
                    <td>
                        Email
                    </td>
                    <td>
                        {user.email}
                    </td>
                </tr>
                <tr>
                    <td>
                        Status
                    </td>
                    <td>
                        
                        {user.status}
                        
                        
                    </td>
                </tr>
                
            </table>
        </div>
    )
}

export default UserDetails