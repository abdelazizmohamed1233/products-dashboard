import './index.css';
import {Link} from 'react-router-dom'
import {Delete} from '@material-ui/icons'
import { usersList } from "../../dummyData";
function Users(){
    return (
        <div>
        <table className='table   table-bordered table-dark '>
        <thead >
                <tr >
                    <th></th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody> 
                
                {usersList.map((user)=>{
                return<tr>
                    <td>
                        <img src={'/images/img_avatar.png'} alt="product-img" className='user-img'/>
                    
                    </td>
                    <td>
                    {user.name}
                    </td>
                    <td>
                    {user.email}
                    </td>
                    <td>
                    {user.status}
                    </td>
                    <td>
                    <Link to={'/user/'+user.id} className="btn btn-success action">
                       Details
                    </Link>
                    <button className="btn btn-primary ms-2 action">
                       edit
                    </button>
                    
                    
                    <button className="btn btn-danger ms-2 action" data-bs-toggle="modal" data-bs-target="#delete-product" >
                      <Delete/>
                    </button>

                

                <div className="modal fade" id="delete-product" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered">
                    <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title" id="exampleModalLabel">Modal title</h5>
                        <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div className="modal-body text-black">
                        Are You Sure You Want To Delete This ?
                    </div>
                    <div className="modal-footer d-flex  justify-content-between px-5">
                        <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="button" className="btn btn-danger">Confirm</button>
                    </div>
                    </div>
                </div>
                </div>
                    </td>
                    </tr>
            })}
                    
               

            </tbody>
            
            
        </table>
        <div className="p-3 btn-container">
            <Link to='/adduser' className='btn btn-success'>
                Add User
            </Link>


            <button className='btn btn-danger' data-bs-toggle="modal" data-bs-target="#clear-users">
                Clear Users
            </button>

            <div className="modal fade" id="clear-users" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered">
                    <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title" id="exampleModalLabel">Modal title</h5>
                        <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div className="modal-body text-black">
                        Are You Sure You Want To Clear All Users ?
                    </div>
                    <div className="modal-footer d-flex  justify-content-between px-5">
                        <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="button" className="btn btn-danger">Confirm</button>
                    </div>
                    </div>
                </div>
                </div>

        </div>
        </div>
    )
}

export default Users;