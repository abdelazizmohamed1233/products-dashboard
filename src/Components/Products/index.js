

import axios from 'axios'
import './index.css'
import {Edit,Delete} from '@material-ui/icons';
import {useState,useEffect} from 'react'
import {Modal,Button} from 'react-bootstrap';
import {Link} from 'react-router-dom'
function Products(){
 



   /*
    modal control functions
    */
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);



    const [update,forceUpdate]=useState(0)



    
    /*
    delete function
    */ 

    function deleteHandler(id){ 
        axios.delete(`https://mini-ecommerce.silicon-arena.com/api/admin/products/${id}`).then(response=>console.log(response));

        handleClose()

    }





        /*
        receive and set products hooks 
        */
     

    const [ourProducts,setProducts]=useState(null);
    useEffect(()=>{
        axios.get(`https://mini-ecommerce.silicon-arena.com/api/products`).then(response=>
    setProducts(response.data))

    },[])

    



    return(
        ourProducts ?
        <div>

        
        <table className='table   table-bordered  table-dark'>
            <thead >
                <tr >
                    <th></th>
                    <th>Name</th>
                    <th>Dimensions</th>
                    <th>Price</th>
                   
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody> 
                {console.log(ourProducts)}
                {ourProducts.map((product)=>{
                return<tr>
                    
                    <td  style={{backgroundColor:product.images[0].background_color}} >
                        
                        <img src={product.images[0]?.img} alt="product-img" className='product-img'/> 
                    
                    </td>
                    <td>
                    {product.name}
                    </td>
                    <td>
                    {product.width} X {product.height}
                    </td>
                    <td>
                    {product.price} $
                    </td>
                    
                    <td>
                    <Link to={'/product/'+product.id} className="btn btn-success action">
                       Details
                    </Link>
                    <button className="btn btn-primary ms-2 action">
                       Edit
                    </button>


         {/* ---- modal start ---- */}
                    
         <button className="btn btn-danger ms-2 action" onClick={handleShow} >
                      <Delete/>
          </button>


         <Modal show={show} onHide={handleClose}  >
            <Modal.Header  >
               <Modal.Title>Delete Product</Modal.Title>
            </Modal.Header>

            <Modal.Body >Are You Sure You Want TO Delete This ?</Modal.Body>

            <Modal.Footer className='d-flex px-5  justify-content-between '>
                <Button variant="secondary" onClick={handleClose}>
                    Close
                </Button>
                <Button variant="danger" onClick={()=>deleteHandler(product.id)}>
                    Confirm
                </Button>
            </Modal.Footer>
      </Modal>

       {/* ---- modal end ---- */}

       </td>
        </tr>
            })}

            </tbody>
        </table>





        {/* adding and clearing buttons */}
        <div className="p-3 btn-container">
            <Link to='/addproduct' className='btn btn-success'>
                Add Product
            </Link>


            <button className='btn btn-danger' data-bs-toggle="modal" data-bs-target="#clear-products">
                Clear Products
            </button>

            <div className="modal fade" id="clear-products" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered">
                    <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title" id="exampleModalLabel">Modal title</h5>
                        <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div className="modal-body text-black">
                        Are You Sure You Want To Clear All Products ?
                    </div>
                    <div className="modal-footer d-flex  justify-content-between px-5">
                        <button type="button" className="btn btn-secondary" data-bs-dismiss="modal" >Close</button>
                        <button type="button" className="btn btn-danger" data-bs-dismiss="modal">Confirm</button>
                    </div>
                    </div>
                </div>
                </div>

        </div>
        </div> : <div></div>
    )
}

export default Products;