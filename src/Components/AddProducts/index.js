import './index.css';
import { useRef, useState,useEffect} from 'react';
import axios from 'axios';
import imageToBase64 from 'image-to-base64/browser';
import { FileCopyOutlined } from '@material-ui/icons';



function AddProduct(){

    






 
    /*
    transform image to base64
    */
    const [imgBase,setImage]=useState() 

    const uploadImage = async (e) => {
        const file = e.target.files[0];
        const base64 = await convertBase64(file);
        setImage(base64);
       
      };
    
      const convertBase64 = (file) => {
        return new Promise((resolve, reject) => {
          const fileReader = new FileReader();
          fileReader.readAsDataURL(file);
    
          fileReader.onload = () => {
            resolve(fileReader.result);
          };
    
          fileReader.onerror = (error) => {
            reject(error);
          };
        });
      };






    /*
    using  useRef hooks to get current value of input fields
    */


     


    const nameRef=useRef();
    const materialRef=useRef()
    const introRef=useRef();
    const descRef=useRef();
    const goodRef=useRef();
    const widthRef=useRef();
    const heightRef=useRef();
    const priceRef=useRef();
    const colorRef=useRef();

    const [product,setProduct]=useState({})
    
    

    function submitHandler(e){
        
        e.preventDefault();
        console.log('submitted');
        

        
        
     
        setProduct({
            material:materialRef.current.value,
            good_to_know:goodRef.current.value,
            name:nameRef.current.value,
            description:descRef.current.value,
            intro:introRef.current.value,
            width:widthRef.current.value,
            height:heightRef.current.value,
            price:priceRef.current.value,
            img:imgBase,background_color:colorRef.current.value


        })
        console.log(product)

         axios.post('https://mini-ecommerce.silicon-arena.com/api/admin/products',product).then(response=>console.log(response))
        
    }
    
    return(
        <div>
               <div className='addproduct container px-5'>
        <h2 className='user-header'>Add New product</h2>
    <form onSubmit={submitHandler}>
    <div className="form-group my-3">
    <label htmlFor="name">Product Name</label>
    <input type="text" className="form-control" id="name"  placeholder="Enter Prodcut Name"    ref={nameRef} required/>
    </div>

    <div className="form-group my-3">
    <label htmlFor="material">Product Materials</label>
    <input type="text" className="form-control" id="material"  placeholder="Enter Prodcut Material" ref={materialRef} required/>
    </div>

    <div className="form-group my-3">
    <label htmlFor="intro"> Intro</label>
    <textarea type="text" className="form-control" id="intro"  placeholder="  Intro " ref={introRef} required/>
    </div>

    <div className="form-group my-3">
    <label htmlFor="description"> Description</label>
    <textarea type="text" className="form-control" id="description"  placeholder="  Description " ref={descRef} required/>
    </div>


    <div className="form-group my-3">
    <label htmlFor="additional"> Good To Know</label>
    <input type="text" className="form-control" id="additional"  placeholder="Enter  Additional Data" ref={goodRef} required/>
    </div>


    <div className="row">
    <div className="form-inline col my-3">
    <label htmlFor="width">Width</label>
    <input type="number" className="form-control" id="width"   placeholder="Enter Product Width" ref={widthRef} min='1' max='3' required/>
    </div>
    <div className="form-inline col my-3">
    <label htmlFor="height">Height</label>
    <input type="number" className="form-control" id="height" placeholder="Enter Product Height" ref={heightRef} min='1' max='3' required />
    </div>
    
    

    </div>
   

    <div className="form-group my-3">
    <label htmlFor="price">Price</label>
        <input type="number" className="form-control" id="price"  placeholder="Enter Product Price" ref={priceRef} min='1' required/>
    </div>

   

    <div className="form-group my-4">
    <label >Choose an image</label>
    <br />
    
    <input type="file"  className="form-control-file"   onChange={uploadImage} required/>
    </div>

    <div className="form-group my-4">
    <label >Choose Backgrund Color</label>
    <br />
    
    <input type="color"  className="form-control-file" ref={colorRef}  required/>
    </div>

  <button type="submit" className="btn btn-primary">Add</button>
    </form>
          
        </div>
        </div>
    )
}

export default AddProduct