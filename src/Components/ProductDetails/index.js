import { productsList } from "../../dummyData"
import './index.css';
import axios from 'axios';
import {useState,useEffect} from 'react';
function ProductDetails(props){
 
   
    
   
      

    const [ourProduct,setProduct]=useState(null);
    useEffect(()=>{
        axios.get(`https://mini-ecommerce.silicon-arena.com/api/products/${props.match.params.id}`).then(response=>
    setProduct(response.data))

    },[])
    
  
    

    


   
    return(
        
        ourProduct ?
        
        
        <div className='product-details text-white py-3 '>
            
          <h3>{ourProduct.name}</h3>
          

        <div  className='img-container my-4' style={{backgroundColor:ourProduct.images[0].background_color}} >
          <img src={ourProduct.images[0]?.img} alt="product-img" className='main-img'/>
          </div>
          
          
            <table className='table table-dark table-bordered product-table text-light '>
                
                <tr>
                    <td>
                        Dimensions
                    </td>
                    <td>
                        {ourProduct.width} X  {ourProduct.height}
                    </td>
                </tr>
               
                <tr>
                    <td>
                        Price
                    </td>
                    <td>
                        
                        {ourProduct.price}
                        
                        
                    </td>
                </tr>
                <tr>
                    <td>
                        Materials
                    </td>
                    <td>
                    {ourProduct.material}
                    </td>
                </tr>
                <tr>
                    <td>Good To Know</td>
                    <td>{ourProduct.description}</td>
                </tr>

            </table>
            
        </div> : <div></div>
    )
}

export default ProductDetails