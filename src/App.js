
import { BrowserRouter as Router, Switch, Route,Link } from "react-router-dom";

import './App.css';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import '../node_modules/bootstrap/js/dist/modal'
import { NotificationsNone} from "@material-ui/icons";
import Topbar from "./Components/Topbar";
import Sidebar from "./Components/Sidebar";
import Products from "./Components/Products";
import Users from "./Components/Users";
import AddUser from "./Components/AddUser";

import AddProduct from "./Components/AddProducts";
import ProductDetails from "./Components/ProductDetails";
import UserDetails from "./Components/UserDetails";





function App() {
  return (
    <Router className="App ">
     <Topbar/>
     <div className="d-flex bg-dark">
       <Sidebar/>
       <div className="content">
        <Route path='/products' component={Products}/>
        <Route path='/users' component={Users}/>
        <Route path='/addproduct' component={AddProduct}/>

        <Route path='/adduser' component={AddUser}/>

      {/* ---important note  هام جدا--- */}

       {/* take care here i did that to send props of route to the component and that only to hold id from it's url to use it to get all details of the same product by it's id */}
       {/* دة كله عشان اجيب ال أى دى  */}


        <Route path='/product/:id' render={(props)=>{
          return <ProductDetails {...props}/>
        }}/>

        <Route path='/user/:id' render={(props)=>{
          return <UserDetails {...props}/>
        }}/>
       </div>




       







      
     </div>
    </Router>
  );
}

export default App;
